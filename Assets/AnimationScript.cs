﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationScript : MonoBehaviour
{
    public Animator _animator;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    public void StartAnimation()
    {
        _animator.SetTrigger("LoadingTrigger");
    }

    public void FinishAnimation()
    {
        _animator.SetTrigger("LoadingFinished");
    }

    public void DetroyCube()
    {
        Destroy(gameObject);
    }
}
