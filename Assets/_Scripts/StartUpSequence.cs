﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartUpSequence : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        SceneLoaderAsync.Instance.LoadScene("Menu");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
