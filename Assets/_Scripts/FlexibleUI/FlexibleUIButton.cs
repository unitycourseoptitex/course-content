﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
[RequireComponent(typeof(Button))]

[ExecuteInEditMode]

public class FlexibleUIButton : FlexibleUI
{

    protected Button button;
    protected Image image;

    public ButtonType buttonType;

    public enum ButtonType
    {
        Default,
        Confirm,
        Decline,
        Warning
    }

    public override void Awake()
    {
        button = GetComponent<Button>();
        image = GetComponent<Image>();

        base.Awake();
    }

    protected override void OnSkinUI()
    {
        button.transition = Selectable.Transition.SpriteSwap;
        button.targetGraphic = image;

        image.sprite = flexibleUIData.buttonSprite;
        image.type = Image.Type.Sliced;
        button.spriteState = flexibleUIData.buttonSpriteState;

        switch (buttonType)
        {
            case ButtonType.Confirm:
                image.color = flexibleUIData.confirmColor;
                break;
            case ButtonType.Decline:
                image.color = flexibleUIData.declineColor;
                break;
            case ButtonType.Default:
                image.color = flexibleUIData.defaultColor;
                break;
            case ButtonType.Warning:
                image.color = flexibleUIData.warningColor;
                break;
        }


        base.OnSkinUI();
    }


    private void Update()
    {
        base.Update();
    }
}