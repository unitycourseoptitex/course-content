﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Efi.Optitex
{
    /// <summary>
    /// example script that counts clicks by direct invokation or event delegation
    /// </summary>
    public class ClickCounter : MonoBehaviour
    {
        [SerializeField]
        private int counter = 0;
        void OnEnable()
        {
            ButtonExample.OnClicked += CountClick;
        }


        void OnDisable()
        {
            ButtonExample.OnClicked -= CountClick;
        }

        public void CountClick()
        {
            counter++; 
        }
    }
}

