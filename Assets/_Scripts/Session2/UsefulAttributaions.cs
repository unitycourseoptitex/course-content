﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Efi.Optitex
{
    public class UsefulAttributaions : MonoBehaviour
    {
        [Serializable]
        public struct Trigger
        {
            [SerializeField]
            public GameObject triggerPrefab;
        }

        #region public variables
        [Header("Very Important Settings")]
        [Tooltip("The time in hours between 0-24")]
        [Range(0f,24f)]
        public float _timeOfDay;

        [Space]
        [Header("Other Settings")]
        [Tooltip("User name (optional)")]
        public string _nameOfUser = "user 1";

        #endregion

        #region private variables
        [SerializeField]
        private float randomValue;
        #endregion

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        [ContextMenu("RandomValue")]
        private void RandomizeValueFromRightClick()
        {
            randomValue = UnityEngine.Random.Range(-5f, 5f);
        }

    }
}

