﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Efi.Optitex
{

    public class GameObjectScaler : MonoBehaviour
    {
        public GameObject _gameObjectToScale;

        void OnEnable()
        {   
            EventManager.StartListening("ImportantEvent", ScaleUp);
        }

        void OnDisable()
        {
            EventManager.StopListening("ImportantEvent", ScaleUp);
        }

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        /// <summary>
        /// Scales up the public gameobject that is attached to the script 
        /// </summary>
        public void ScaleUp()
        {
            Vector3 localScale = _gameObjectToScale.transform.localScale;
            if (_gameObjectToScale != null)
            {
                _gameObjectToScale.transform.localScale = new Vector3(localScale.x * 2, localScale.y * 2, localScale.z * 2);
            }
        }
    }

}
