﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameObjectSpaner : MonoBehaviour
{
    public GameObject _prefab;

    private Camera camera;

    private void Start()
    {
        camera = Camera.main;
        Sanity();
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("Left mouse clicked");
            RaycastHit hit;
            Ray ray = camera.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform.name == "Plane")
                {
                    Instantiate(_prefab, hit.point, Quaternion.identity);
                }
            }
        }
    }

    private void Sanity()
    {
        if (_prefab == null)
            Debug.LogError("prefab is missing", gameObject);
    }
}
