﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Efi.Optitex
{
    public class Timer : MonoBehaviour
    {

        public UnityEvent ImportantEvent;

        // Start is called before the first frame update
        void Start()
        {
            StartCoroutine(FireImportantEventWithDelay());
        }

        // Update is called once per frame
        void Update()
        {

        }

        /// <summary>
        /// Invokes an "ImportantEvent" signal
        /// </summary>
        /// <returns></returns>
        private IEnumerator FireImportantEventWithDelay()
        {
            yield return new WaitForSeconds(2f);
            EventManager.TriggerEvent("ImportantEvent");
            if (ImportantEvent != null)
                ImportantEvent.Invoke();
        }
    }
}

