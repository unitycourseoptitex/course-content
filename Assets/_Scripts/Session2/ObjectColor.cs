﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Profiling;

namespace Efi.Optitex
{
    public class ObjectColor : MonoBehaviour, IObjectColor
    {
        [Tooltip("The object that will be colored on important event")]
        public GameObject _objectToColor;
        [Tooltip("The color to change to on event")]
        public Color _colorToChange;

        void OnEnable()
        {
            EventManager.StartListening("ImportantEvent", testFunction);
        }

        void OnDisable()
        { 
            EventManager.StopListening("ImportantEvent", testFunction);
        }
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        private void testFunction()
        {

        }

        public bool ChangeColor()
        {
            Profiler.BeginSample("MyPieceOfCode");
            if (_objectToColor)
            {
                //Fetch the Renderer from the GameObject
                Renderer rend = _objectToColor.GetComponent<Renderer>();
                if (rend == null)
                    return false;
                //Set the main Color of the Material to green
                rend.material.color = _colorToChange;
                return true;
            }
            return false;
        }
    }

}
