﻿namespace Efi.Optitex
{
    public interface IObjectColor
    {
        bool ChangeColor();
    }
}