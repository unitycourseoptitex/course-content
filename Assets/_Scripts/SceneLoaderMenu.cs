﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneLoaderMenu : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void LoadLevel(string name)
    {
        SceneLoaderAsync.Instance.LoadScene(name, gameObject.scene.name);
    }
}
