﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Profiling;

public class SlowScript : MonoBehaviour
{
    int i = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        i++;
        if (i % 60 == 0) AnotherLoop();
    }

    void AnotherLoop()
    {
        Profiler.BeginSample("My Sample");
        for (int i = 0; i < 50; i++)
        {
            Debug.Log("loop");
        }
        Profiler.EndSample();
    }
}
