﻿// ----------------------------------------------------------------------------
// Unite 2017 - Game Architecture with Scriptable Objects
// 
// Author: Ryan Hipple
// Date:   10/04/17
// ----------------------------------------------------------------------------

using UnityEngine;
using UnityEngine.Profiling;

namespace RoboRyanTron.Unite2017.Variables
{
    public class SimpleUnitHealth : MonoBehaviour
    {
        public FloatVariable HP;

        public bool ResetHP;

        public FloatReference StartingHP;

        private void Start()
        {
            if (ResetHP)
                HP.SetValue(StartingHP);
        }
        private void OnTriggerEnter(Collider other)
        {
            //Profiler.BeginSample("MyPieceOfCode");
            DamageDealer damage = other.gameObject.GetComponent<DamageDealer>();
            if (damage != null)
                HP.ApplyChange(-damage.DamageAmount);
            Debug.Log("OnTriggerEnter",gameObject);
            //Profiler.EndSample();
          //  Debug.Break();

        }

        [ContextMenu("Manual Test")]
        public void Test()
        {
            Debug.Log("Hello World");
        }
    }
}