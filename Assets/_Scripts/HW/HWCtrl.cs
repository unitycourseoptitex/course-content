﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HWCtrl : MonoBehaviour
{

    public GameObject _gameObject;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            ToggleGameobject();
        }
    }

    public void ToggleGameobject()
    {
        if (_gameObject)
            _gameObject.SetActive(!_gameObject.activeSelf);
    }
}
